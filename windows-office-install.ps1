
$LOGFILE = "c:\logs\plugin-windows-office-install.log"

Function Write-Log([String] $logText) {
  '{0:u}: {1}' -f (Get-Date), $logText | Out-File $LOGFILE -Append
}

Function add-regisrty ($registryPath, $regname, $value) {
  # Create the key if it does not exist
  If (-NOT (Test-Path $RegistryPath)) {
    New-Item -Path $RegistryPath -Force | Out-Null
  }  
  # Now set the value
  New-ItemProperty -Path $RegistryPath -Name $regname -Value $Value -PropertyType DWORD -Force 
}

Function Main {
  $office_temp = "$Env:temp\officeinstall"
  New-Item -ItemType Directory -Path $office_temp -Force

  Write-Log "Start plugin-windows-office-install"

  $disable_onedrive = [System.Environment]::GetEnvironmentVariable('disable_onedrive')
  $disable_teams = [System.Environment]::GetEnvironmentVariable('disable_teams')
  $co_disable_msofficeautosave = [System.Environment]::GetEnvironmentVariable('co_disable_msofficeautosave')
  
  Write-Log "disable_onedrive = $disable_onedrive"
  Write-Log "disable_teams = $disable_teams"
  Write-Log "co_disable_msofficeautosave = $co_disable_msofficeautosave"

  Copy-Item "$PSScriptRoot\config.xml" -Destination "$office_temp\config.xml"

  Write-Log "Change config.xml"
 
  $xmlDoc = [System.Xml.XmlDocument](Get-Content $office_temp\config.xml);
    
  $productNode = $xmlDoc.SelectSingleNode("//Configuration/Add/Product[@ID='O365ProPlusRetail']")

  if( $disable_onedrive -ne "false" ){
    Write-Log "disable_onedrive in config"
    $newExcludeApp = $xmlDoc.CreateElement("ExcludeApp")
    $newExcludeApp.SetAttribute("ID","OneDrive")
    $productNode.AppendChild($newExcludeApp)

    $newExcludeApp = $xmlDoc.CreateElement("ExcludeApp")
    $newExcludeApp.SetAttribute("ID","Groove")
    $productNode.AppendChild($newExcludeApp)
  }

  if( $disable_teams -ne "false" ){
    Write-Log "disable_teams in config"
    $newExcludeApp = $xmlDoc.CreateElement("ExcludeApp")
    $newExcludeApp.SetAttribute("ID","Teams")
    $productNode.AppendChild($newExcludeApp)

    $newExcludeApp = $xmlDoc.CreateElement("ExcludeApp")
    $newExcludeApp.SetAttribute("ID","Lync")
    $productNode.AppendChild($newExcludeApp)
  }

  $xmlDoc.Save("$office_temp\config.xml")

  $url = "https://officecdn.microsoft.com/pr/wsus/setup.exe"
  $outpath = "$office_temp/setup.exe"
  $wc = New-Object System.Net.WebClient
  $wc.DownloadFile($url, $outpath)

  Write-Log "Run installler"

  $argsConf = @("/configure", "$office_temp\config.xml")

  try {
    Start-Process -Filepath "$office_temp\setup.exe" -ArgumentList $argsConf -Wait
  }
  catch {
    Write-Log "$_"
    Throw $_
  }
  
  $winprodname = (Get-ItemProperty "HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion").ProductName
  Write-Log "windows version: $winprodname"

  if (($winprodname -like "*server 2019*") -AND ( $disable_teams -eq "false" )) {
    Write-Log "Install MS Teams"
    New-ItemProperty -Path HKLM:\Software\Policies\Microsoft\Windows\Appx -Name AllowAllTrustedApps -PropertyType DWORD -Value 1
    Invoke-GPUpdate -Force

    $url =   'https://msedge.sf.dl.delivery.mp.microsoft.com/filestreamingservice/files/304fddef-b073-4e0a-b1ff-c2ea02584017/MicrosoftEdgeWebview2Setup.exe'
    $outpath = "$office_temp/MicrosoftEdgeWebview2Setup.exe"
    $wc = New-Object System.Net.WebClient
    $wc.DownloadFile($url, $outpath)

    Start-Process -Filepath "$office_temp\MicrosoftEdgeWebview2Setup.exe" -ArgumentList "/silent /install"

    $url = "https://statics.teams.cdn.office.net/production-windows-x64/enterprise/webview2/lkg/MSTeams-x64.msix"
    $outpath = "$office_temp/MSTeams-x64.msix"
    $wc = New-Object System.Net.WebClient
    $wc.DownloadFile($url, $outpath)
    
    DISM /Online /Add-ProvisionedAppxPackage /PackagePath:$office_temp\MSTeams-x64.msix /SkipLicense
  }
  
  Write-Log "remove installation files"

  Remove-Item -Path $office_temp -Recurse -Force

  try {
    if ($co_disable_msofficeautosave -ne "false" ){

      Write-Log "disable autosave"
      New-PSDrive -PSProvider Registry -Name HKU -Root HKEY_USERS
      REG LOAD "HKU\temphive" C:\users\default\ntuser.dat
      
      add-regisrty "HKU:\temphive\Software\Policies\Microsoft\Office\16.0\Word" "AutosaveByDefaultAdminChoice" "2"
      add-regisrty "HKU:\temphive\Software\Policies\Microsoft\Office\16.0\Word" "AutoSaveByDefaultUserChoice" "2"

      add-regisrty "HKU:\temphive\Software\Policies\Microsoft\Office\16.0\Excel" "AutosaveByDefaultAdminChoice" "2"
      add-regisrty "HKU:\temphive\Software\Policies\Microsoft\Office\16.0\Excel" "AutoSaveByDefaultUserChoice" "2"

      add-regisrty "HKU:\temphive\Software\Policies\Microsoft\Office\16.0\Powerpoint" "AutosaveByDefaultAdminChoice" "2"
      add-regisrty "HKU:\temphive\Software\Policies\Microsoft\Office\16.0\Powerpoint" "AutoSaveByDefaultUserChoice" "2"

      [gc]::Collect()
      Start-Sleep -Seconds 2
      REG UNLOAD "HKU\temphive"
    }  
  }
  catch {
    Write-Log "$_"
    Throw $_
  }

  Write-Log "End plugin-windows-office-install"
 
}

Main